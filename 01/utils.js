const Movie = (props) => {
    return (
        <a href="#" className="movie">
                <h3>{props.name}</h3>
                <p>{props.year}</p>
                <img src={props.img} alt="image" className="image"/>
        </a>
    );
};

const container = document.getElementById('app');

const movies = (
    <React.Fragment>
        <Movie name="Остров собак" year="2018" img="https://www.kinopoisk.ru/images/film_big/939785.jpg"/>
        <Movie name="Борат" year="2006" img="https://www.kinopoisk.ru/images/film_big/102474.jpg"/>
        <Movie name="Авиатор" year="2004" img="https://www.kinopoisk.ru/images/film_big/6374.jpg"/>
        <Movie name="Kung Fury" year="2015" img="https://www.kinopoisk.ru/images/film_big/824954.jpg"/>
        <Movie name="Убийство в Восточном экспрессе" year="2017" img="https://www.kinopoisk.ru/images/film_big/817969.jpg"/>
        <Movie name="'1408'" year="2007" img="https://www.kinopoisk.ru/images/film_big/195563.jpg"/>
        <Movie name="Выживут только любовники" year="2013" img="https://www.kinopoisk.ru/images/film_big/565819.jpg"/>
        <Movie name="" year="" img=""/>
    </React.Fragment>
);

ReactDOM.render(movies, container);