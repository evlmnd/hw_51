const container = document.getElementById('app');

const NavLink = (props) => {
    return (
        <li>
            <a href="#" className="nav-link">{props.text}</a>
        </li>
    );
};

const NavLogo = (props) => {
    return (
        <a href="#" className="logo">{props.logo}</a>
    )
};

const header = (
    <React.Fragment>
        <header>
            <NavLogo logo="Logo."/>
            <ul className="main-nav">
                <NavLink text="About us"/>
                <NavLink text="News"/>
                <NavLink text="Links"/>
                <NavLink text="Shop"/>
                <NavLink text="Help"/>
                <NavLink text="Contact"/>
            </ul>
        </header>
    </React.Fragment>
);

ReactDOM.render(header, container);
